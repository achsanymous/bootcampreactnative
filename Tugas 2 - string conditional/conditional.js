//======TUGAS 1 conditional============================================================================================================
var nama = "john"
var peran = "penyihir"
var int = 0

if ( nama == "" ) {
    console.log("Nama harus diisi!")
   
} else {
    console.log("Selamat datang di Dunia Werewolf," + nama);
    if (peran == "penyihir" ) {
        console.log("Halo Penyihir " + nama +", kamu dapat melihat siapa yang menjadi werewolf!")    
    } else if ( peran == "guard" ) {
        console.log("Halo Guard " + nama +", kamu akan membantu melindungi temanmu dari serangan werewolf.")
    } else if( peran == "werewolf") {
        console.log("Halo Werewolf " + nama +", Kamu akan memakan mangsa setiap malam!")
    } else if( peran == "") {
        console.log("Halo " + nama +", Pilih peranmu untuk memulai game!")
    } else {
        console.log("")
    }
    
}

console.log("")
//======TUGAS 2 conditional============================================================================================================
var tanggal = 1; // assign nilai variabel tanggal disini! (dengan angka antara 1 - 31)
var bulan = 2; // assign nilai variabel bulan disini! (dengan angka antara 1 - 12)
var tahun = 1999; // assign nilai variabel tahun disini! (dengan angka antara 1900 - 2200)

switch(bulan) {
  case 1:   { console.log(tanggal + ' januari ' + tahun); break; }
  case 2:   { console.log(tanggal + ' februari ' + tahun); break; }
  case 3:   { console.log(tanggal + ' maret ' + tahun); break; }
  case 4:   { console.log(tanggal + ' april ' + tahun); break; }
  case 5:   { console.log(tanggal + ' mei ' + tahun); break; }
  case 6:   { console.log(tanggal + ' juni ' + tahun); break; }
  case 7:   { console.log(tanggal + ' juli ' + tahun); break; }
  case 8:   { console.log(tanggal + ' agustus ' + tahun); break; }
  case 9:   { console.log(tanggal + ' september ' + tahun); break; }
  case 10:   { console.log(tanggal + ' oktober ' + tahun); break; }
  case 11:   { console.log(tanggal + ' november ' + tahun); break; }
  case 12:   { console.log(tanggal + ' desember ' + tahun); break; }
  default:  { console.log(' Tidak terjadi apa-apa '); }}

