console.log('LOOPING PERTAMA');


for(var deret = 2; deret < 22; deret += 2) {
    console.log(deret + ' - I love coding');
  }
   
  console.log('LOOPING KEDUA');
   
  for(var deret = 20; deret > 0; deret -= 2) {
    console.log(deret + ' - I will become a mobile developer');
  } 


  //-------------------------------------------------------------------------------------------


for (var nilai = 1; nilai <= 20; nilai++){
    if (nilai%2 != 0 && nilai%3 === 0) {
            console.log(nilai +  " - I Love Coding");
    }
    else if (nilai%2 == 0) {
            console.log(nilai + " - Berkualitas");   
    }
    else if (nilai%2 != 0) {
        console.log(nilai + " - Santai");
    }
}

//-------------------------------------------------------------------------------------------

var baris = "";
for ( a = 1; a <= 4; a++){
  for (b = 1; b <= 8; b++){
    baris += "#";
  }
  console.log(baris);
  baris = "";
}
//-------------------------------------------------------------------------------------------


var baris2 = "";
for ( x = 1; x <= 8; x++){
  baris2 += "#";
  
  console.log(baris2);
 
}

//-------------------------------------------------------------------------------------------

var ukuran = 8; 
var papan = "";

for (var p = 0; p < ukuran; p++) {
  for (var q = 0; q < ukuran; q++) {
    if ((p + q) % 2 == 0)
      papan += " ";
    else
      papan += "#";
  }
  papan += "\n";
}

console.log(papan);